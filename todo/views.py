from flask import Flask, Blueprint, render_template, request, flash, session, url_for, redirect
from pymongo import MongoClient
from .db import *
from werkzeug.utils import secure_filename
from werkzeug.security import check_password_hash, generate_password_hash

#bp = Blueprint('auth', __name__, url_prefix='/auth')

app = Flask(__name__)
app.secret_key = 'super secret key'



# Config options - Make sure you created a 'config.py' file.
app.config.from_object('config')
# To get one variable, tape app.config['MY_VARIABLE'] 

#client=MongoClient(app.config['mongoDB'])
#db= client.todolistflask
#tasks= db.task

@app.route('/register', methods=('GET', 'POST'))
def register():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        
        error = None

        if not username:
            error = 'Username is required.'
        elif not password:
            error = 'Password is required.'
        #elif session['user']==! None:
        #	error = "You are already logged in"

        if error is None:
        	try:
        		db.user.insert({'username':username, 'password':generate_password_hash(password)})
        	except:
        		print('failed to create user in db')

        	return redirect(url_for('login'))
        else:

        	flash(error)

    return render_template('register.html')

@app.route('/login', methods=('GET', 'POST'))
def login():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        
        error = None

        if not username:
            error = 'Username is required.'
        elif not password:
            error = 'Password is required.'
        #elif session['user']==! None:
        #	error = "You are already logged in"

        if error is None:
        	try:
        		searchedUser = db.user.find_one({'username':username})
        	except:
        		print('failed to find user in db')
        		error= "User not found"
        	if searchedUser['username'] == username and check_password_hash(searchedUser['password'], password) == True: 
        		session['user']= username
        		return redirect(url_for('index'))
        	else:
        		error = "Wrong credentials -- for Dim only Nikoumouk"

        	flash(error)
    return render_template('login.html')

@app.route('/')
def index():
	#all_my_tasks = tasks.find()
	all_my_category = db.category.find()
	all_my_status= db.status.find()
	all_my_priority=db.priority.find()
	all_my_tasks= db.task.find()
	print(tasks)
	print(all_my_tasks)

	categories = []
	for document in all_my_category:
		document['_id'] = str(document['_id'])
		categories.append(document)


	status = []
	for document in all_my_status:
		document['_id'] = str(document['_id'])
		status.append(document)


	priorities = []
	for document in all_my_priority:
		document['_id'] = str(document['_id'])
		priorities.append(document)

	response = []
	for document in all_my_tasks:
		document['_id'] = str(document['_id'])
		response.append(document)

	if session['user'] == None:
		loggeduser= session['user']
	else:
		loggeduser= "anonymous"

	return render_template('index.html',
    						koala='grillon',
    						data=response,
    						taskCategories=categories,
    						taskPriorities=priorities,
    						taskStatus=status,
    						visitor=loggeduser)

@app.route('/newtask',methods=['POST'])
def newtask():
	taskName=request.form['name']
	taskPriority=request.form['priority']
	taskStatus=request.form['status']
	taskCategory=request.form['category']
	try:
		db.task.insert({'name':taskName, 'priority':taskPriority, 'status':taskStatus, 'category':taskCategory, 'subtasks':[]})
	except:
		print('failed to push in db')

	return redirect(url_for('index'))



@app.route('/killtask/<taskName>')
def killtask(taskName):
	print(taskName)
	targetedTask= db.task.find_one({'name':taskName })
	
	try:
		db.task.remove({'name': taskName })
	except:
		print('failed to kill task in db ')

	return redirect(url_for('index'))


@app.route('/seetask/<taskName>')
def seetask(taskName):
	print(taskName)
	
	try:
		targetedTask= db.task.find_one({'name':taskName })
	except:
		print('failed to find task in db ')

	return render_template('test.html', task=targetedTask)


@app.route('/addsubtask',methods=['POST'])
def addsubtask():
	
	subtaskName=request.form['name']
	subtaskParent=request.form['parentTask']
	subtaskDescription=request.form['desc']
	subtaskPicure=request.form['picture']

	try:
		parentTask= db.task.find_one({'name':subtaskParent })
	except:
		print('failed to find parenttask in db ')
	try:
		db.task.update({'name' : parentTask['name']}, {"$set" : {'subtasks' : [{'name': subtaskName, 'description': subtaskDescription, 'picture': 'https://images.unsplash.com/photo-1569335546324-3b58b094f0b5?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60'}]}})
		#db.task.update({'name' : parentTask['name']}, {'subtasks' : [{'name': subtaskName, 'description': subtaskDescription}]})
	except:
		print('update failed')


	return redirect(url_for('seetask', taskName= parentTask['name']))



if __name__ == "__main__":
	app.run()


#{'_id': ObjectId('5eaa979d80b1b124eaf46914'), 'name': 'Cooing my lama in a disgustingly sweet voice', 'priority': 'Just an afterthought', 'status': 'Ongoing', 'category': 'Mildly weird', 'subtasks': [{'name': 'go to the himalayah', 'description': 'climb to the top and catch a yak', 'picture': 'https://images.unsplash.com/photo-1569335546324-3b58b094f0b5?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60'}]} 




