#! /usr/bin/env python
import todo
from todo import app

if __name__ == "__main__":
    app.run(debug=True)